# coding: utf-8
from datetime import datetime
import random

#Variables du fraculateur
x1y1 = 0
x1y2 = 0
x1y3 = 0
x1y0 = 0
A = 0
total = 0
power = False
level = 1
music = True

Mortys = False


class chrono(object):
	def __init__(self,t=30):
		self.minutes = t / 60
		t = t - 60 *(t/60)
		self.secondes = t

	def dec(self):
		self.secondes -= 1
		if self.secondes < 0: 
			self.minutes -= 1 
			if self.minutes>-1:
				self.secondes = 59
				if self.minutes<0:
					self.secondes = 0
					self.minutes = 0 
				
		
	def get(self):
		return self.minutes,self.secondes

	def get_format(self):
		return str(self.minutes)+":"+str(self.secondes)

	def is_null(self):
		return self.minutes+self.secondes <= 0

chronometre = chrono(120)
"""
chronometre = FRAC.chrono(65)
chronometre.dec()
M,S = chronometre.get()
label.text = chronometre.get_format()

"""


def nouvelle_seed():
	random.seed(datetime.now())

nouvelle_seed()


class RMPEQ(object):
	def __init__(self):
		self.C = random.choice([-5,-4,-3,-2,-1,1,2,3,4,5])
		self.PV = 20.00

	def gameiswin(self):
		if self.PV >= 100:
			return True
		return False

	def gameisover(self):
		if self.PV <= 0:
			return True
		return False

	def PVmod(self,val):
		self.PV += float(val) 
		if self.PV > 100:
			self.PV = 100.00
		if self.PV < 0:
			self.PV =0.00

	def fluctuation(self,facteur=1):
		return random.randint(-facteur*3,facteur*2)

	def simulate_fluctuation(self,facteur):
		F = self.fluctuation(facteur)
		if F < 0:
			modval = -3
		else:
			modval = 3
		F = F % modval
		self.C += F
		if self.C < 0:
			modval = -13
		else:
			modval = 13
		oldC = self.C
		self.C = self.C % modval
		if self.C == 0:
			if oldC == modval:
				self.C = oldC
		return F

	def atteinte(self,value):
		value = abs(value)
		if value == 0 :
			self.PVmod(1)
		if value > 11 :
			self.PVmod(-0.5)
		else:
			self.PVmod(-(2*(value/100.0)))
