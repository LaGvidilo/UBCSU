# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['/Users/peignard/Documents/gitlab/UBCSU'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['_tkinter', 'Tkinter', 'twisted'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
a.datas += [('asciinator.py','asciinator.py','DATA')]
a.datas += [('assets.py','assets.py','DATA')]
a.datas += [('autoload.py','autoload.py','DATA')]
a.datas += [('evaluator.py','evaluator.py','DATA')]
a.datas += [('FluoSQL.py','FluoSQL.py','DATA')]
a.datas += [('fraculateur.py','fraculateur.py','DATA')]
a.datas += [('GP.py','GP.py','DATA')]
a.datas += [('KICOMIC_.TTF','KICOMIC_.TTF','DATA')]
a.datas += [('life.py','life.py','DATA')]
a.datas += [('memory.py','memory.py','DATA')]
a.datas += [('roleplay.py','roleplay.py','DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='UBCSU',
          debug=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,Tree('data/', prefix='data/'),Tree('_tmp_/', prefix='_tmp_/'),Tree('dialogues/', prefix='dialogues/'),Tree('captcha_/', prefix='captcha_/'),Tree('mem/', prefix='mem/'),
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='UBCSU')
app = BUNDLE(coll,
             name='UBCSU.app',
             icon="data/icon.png",
             bundle_identifier=None)
