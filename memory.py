#coding: utf-8
#hippocampe
#VERSION: 0.1.4
import ast
def read_file(file):
	f = open(file,"r")
	R=f.read()
	f.close()
	return R

class DBDICT(object):
	def __init__(self,filepath=None):
		self.data = {}
		self.id = 1
		if filepath != None:
			self.data = ast.literal_eval(read_file(filepath))
			if len(self.data.keys())>0:
				self.id = max(self.data.keys())+1
			else:
				self.id = 1

	def init(self):
		self.data = {}
		self.id = 1

	def getter(self,id_):
		return self.data.get(id_)

	def setter(self,id_,**kwargs):
		self.data[id_] = kwargs

	def insert(self,**kwargs):
		self.data[self.id] = kwargs
		self.id += 1

	def delete(self,id_):
		if self.data.get(id_)!=None:
			del self.data[id_]

	def find(self,**kwargs):
		PTS = 0
		RESULT = []

		for k1,v1 in self.data.iteritems():
			PTS = 0
			for k2,v2 in kwargs.iteritems():
				if v1.get(k2)!=None:
					if v1[k2] == v2: PTS+=1
			if PTS >= len(kwargs.keys()):
				RESULT.append(k1)

		return RESULT

	def select(self,**kwargs):
		PTS = 0
		RESULT = []

		for k1,v1 in self.data.iteritems():
			PTS = 0
			for k2,v2 in kwargs.iteritems():
				if v1.get(k2)!=None:
					if v1[k2] == v2: PTS+=1
			if PTS >= len(kwargs.keys()):
				RESULT.append(self.data[k1])

		return RESULT

	def save(self,filepath="mem/game.dict"):
		f = open(filepath,'w+b')
		f.write(str(self.data))
		f.close()

from datetime import datetime
class MEM:
	class talk(object):
		def __init__(self,filepath=None):
			self.DBD = DBDICT(filepath)

		def notforgot(self,inputs="",context="",defcon=5,output="",timestamp=str(datetime.now())):
			#Pour se souvenir de la conversation au stade n
			self.DBD.insert(inputs=inputs,context=context,defcon=defcon,output=output,timestamp=timestamp)
			self.DBD.save("cerveau/memoire/parole.dict")

		def remind(self,inputs=None,context=None,defcon=None,output=None,timestamp=None):
			R = "RESULT = self.DBD.select("
			if bool(inputs):
				R=R+"inputs=inputs"
			if bool(context):
				R=R+"context=context"
			if bool(defcon):
				R=R+"defcon=defcon"
			if bool(output):
				R=R+"output=output"
			if bool(timestamp):
				R=R+"timestamp=timestamp"
			R=R+")"
			exec(R)
			return RESULT


		#==========LOW LEVEL
		def getall(self):
			return self.DBD.data

		#==========DANGER - RISQUE DE CORRUPTION==========
		#----|: A UTILISER EN CAS D'EXTREME URGENCE
		#		SI L'IA DEVIENS INCONTROLABLE ET QU'ELLE
		#		VEUX FAIRE DU MAL OU QU'ELLE DEVIENS FOLLE
		#		D'AUTRES CAS PEUVENT JUSTIFIER D'EFFACER LA
		#		MEMOIRE DE L'IA.
		#(pour ma part je suis assez méchant et je ferait des expériences)
		def init_and_forgot(self):
			#Pour effacer toutes les données courantes sur la conversation
			self.DBD.init()
			self.DBD.save("cerveau/memoire/parole.dict")

		def forgot_inputs(self,inputs=""):
			#Pour faire oublier que l'on a dit un truc
			self.DBD.delete(self.DBD.find(inputs=inputs))
		#==========DANGER - RISQUE DE CORRUPTION==========



