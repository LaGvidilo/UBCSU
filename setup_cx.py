application_title = "UBCSU" #what you want to application to be called
main_python_file = "main.py" #the name of the python file you use to run the program
#import os
#os.environ['TCL_LIBRARY'] = "/System/Library/tcl/8.4"
#os.environ['TK_LIBRARY'] = "/System/Library/tcl/8.4"
import os.path
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')
import sys
#['py2app','kivy','rsa','captcha','bottle','overload','whatever','geocoder'] 

from cx_Freeze import setup, Executable

base = None
if sys.platform == "win32":
    base = "Win32GUI"

includes = ['kivy','rsa','captcha','bottle','overload','whatever','geocoder','statistics'] 

setup(
        name = application_title,
        version = "1.5.5",
        description = "Ultimate Brain Challenge for Super User",
        options = {"build_exe" : {"includes" : includes, "excludes": ["tcl","Tcl","tkinter", "Tkinter", "Tcl"] }},
        executables = [Executable(main_python_file, base = base)])