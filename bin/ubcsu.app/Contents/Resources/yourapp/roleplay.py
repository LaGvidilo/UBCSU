#coding:utf-8

class perso(object):
	def __init__(self,MAXPV=18,MAXPA=25,PV=18,PA=25,EXP=0,LVL=1,range_ATK=2,range_MVE=2):
		self.MAXPV = MAXPV
		self.MAXPA = MAXPA
		self.PV = PV
		self.PA = PA
		self.EXP,self.LVL = EXP,LVL
		self.range_ATK,self.range_MVE = range_ATK,range_MVE

	def PV_x(self,x):
		self.PV += x
		self.PV = 0 if self.PV < 0 else self.PV 

	def PV_init(self):
		self.PV = MAXPV

	def PV_get(self):
		return self.PV,self.MAXPV

	def PA_x(self,x):
		self.PA += x

	def PA_get(self):
		return self.PA

	def PA_init(self):
		self.PA = MAXPA

	def EXP_x(self,x):
		self.EXP+=x
		if self.EXP>1000:
			self.EXP=0
			self.LVL += 1

	def EXP_get(self):
		return self.EXP

	def LVL_get(self):
		return self.LVL

	def LVL_x(self,x):
		self.LVL += x

	def rangeATK_x(self,x):
		self.range_ATK += x

	def rangeATK_get(self):
		return self.range_ATK
	
	def rangeMVE_x(self,x):
		self.range_MVE += x

	def rangeMVE_get(self):
		return self.range_MVE