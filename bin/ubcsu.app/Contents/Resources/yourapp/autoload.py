# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
__version__ = "1.2.0"

import hashlib
import os

#AUTOLOAD by LAGVIDILO
class autoload(object):
	def cherche(self,dir):
		FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
		return FichList

	def read_file(self,file):
		f = open(file,"r")
		R=f.read()
		f.close()
		return R

	def load_html(self,dir="pages"):
		FL = self.cherche(dir)
		R={}
		for f in FL:
			if f.split('.')[1]=="html":
				BUFF = self.read_file(dir+"/"+f)
				R[f.split('.')[0]] = BUFF
		return R

	def load_tpl(self,dir="pages"):
		FL = self.cherche(dir)
		R={}
		for f in FL:
			if f.split('.')[1]=="tpl":
				BUFF = self.read_file(dir+"/"+f)
				R[f.split('.')[0]] = BUFF
		return R

	def load_ext(self,dir="pages",ext="png",level="1",sep="_"):
		FL = self.cherche(dir)
		R={}
		for f in FL:
			if f.split('.')[1]==ext:
				if "level"+level+sep in f.split('.')[0]:
					print "LOAD:",f
					BUFF = self.read_file(dir+"/"+f)
					R[f.split('.')[0]] = BUFF
		return R