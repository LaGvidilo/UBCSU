#coding: utf-8
string = "12345"
F = []
for i in string:
	F.append(ord(i))
maxval=256
LENDNA = 5
import random
import base64
from datetime import datetime
class individu(object):
	def __init__(self,adn=""):
		prot = range(0,256)
		self.DNA = str(adn)
		#print "individu avec ADN:",self.DNA
		self.pts = 0
		random.seed(datetime.now())
		self.name = str(random.randint(0,1000000000))
		if len(adn)==0: 
			for i in range(0,LENDNA):
				self.DNA = self.DNA + chr(abs(random.choice(prot)))

	def get_dna(self):
		return self.DNA


class population(object):
	def __init__(self,pop=10000,goal=F):
		self.indivs = []
		self.goal = goal
		self.fitdata = {}
		for i in range(0,pop):
			self.indivs.append(individu())
			print "CREATION INDIVIDU(",i,"/",pop,")"

	def test_all(self):
		score = {}
		genome = {}
		#print "TEST LEN:",len(self.indivs)
		for i in self.indivs:
			i.new={}
			o=0
			#random.seed(i.get_dna())
			for j in self.goal:
				#random.seed(i.get_dna()[o-1:o])
				o = (o + 1)%LENDNA
				#print "i.get_dna():",i.get_dna()
				random.seed(i.get_dna()[o])
				if i.name not in i.new.keys(): i.new[i.name] = []
				
				if random.randint(0,maxval)==j:
					i.pts += 1 
					i.new[i.name].append(i.get_dna()[o])
					#print i.pts,";",i.name
					score[i.pts] = i.name
					genome[str(i.pts)] = i
				else:
					i.new[i.name].append('-1')


		#print "T:", max(score.keys()),":",score[max(score.keys())],",",base64.b64encode(genome[str(max(score.keys()))].get_dna())
				

	def test(self):
		#print "TEST LEN:",len(self.indivs)
		for i in self.indivs:
			pts=0
			o=2
			#random.seed(i.get_dna())
			for j in self.goal:
				#random.seed(i.get_dna()[o-1:o])
				o = (o + 1)%LENDNA
				random.seed(i.get_dna()[o])
				
				if random.randint(0,maxval)==j:
					pts += 1 

			break
		print "PTS:",pts

	def substitu(self):
		R = {}
		for i in self.indivs:
			n=0
			for j in i.new[i.name]:
				if j!='-1':
					R[n] = j
					break
				n+=1

		T = []
		for i in R.keys():
			T.append(R[i])
		return T

	def evolve(self):
		n=0
		DNA = ""
		u = self.substitu()
		print "U:",u
		for i in u:				

			DNA = DNA + i

			n+=1

		#for i in self.newind:
		self.indivs.append(individu(DNA))
		#print "EVOLVE:",base64.b64encode(DNA)

	def fetch(self,pos,indivs):
		for i in self.indivs:
			print [i.new]
			if i.new[i.name][pos] != "-1": return i.new[i.name][pos]

	def less(self):
		self.newind = []
		print "before less:",len(self.indivs)
		ind = []
		for i in self.indivs:
			if i.new[i.name].count("-1")>len(string)-1:
				#print "suppression de i:", i
				pass
			else:
				ind.append(i)
				print i.new[i.name],"score:",i.pts
				self.newind.append(i.new[i.name])

		print "after less:",len(self.newind)
		"""
		DNA = ""
		for i in self.indivs:
			k=0
			if i.new[i.name].count(-1)>len(string)-1: 
				for j in i.new[i.name]:
					if j == -1:
						j=self.fetch(k,i)
					k+=1
		"""

def generate(DNA,LENSTR=3):
	BUFF = ""
	for i in DNA:
		random.seed(i)
		BUFF=BUFF+chr(random.randint(0,maxval))
	return BUFF


P = population()
P.test_all()
P.less()
P.evolve()

print P.indivs[-1].DNA
print "BUFF:",generate(P.indivs[-1].DNA)

