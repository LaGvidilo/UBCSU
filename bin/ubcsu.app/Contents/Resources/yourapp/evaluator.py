#coding: utf-8
#=============
#BY Rick Sanchez
#This code is for science!
#=============
from datetime import datetime
class collecteur(object):
	def __init__(self):
		self.data = {1:[],2:[],3:[],4:[]}
		self.name = str(datetime.now())

	def add(self,**kwargs):#level,step,state,datetime
		self.data[kwargs['level']].append({'step':kwargs['step'],'state':kwargs['state'],'datetime':kwargs['datetime_']})

	def	get(self,level=1):
		return self.data[level]

	def ratio_progress(self,level=1):
		try:
			step = self.data[level][-1]['step']
			return int(100*(step*16.0))
		except:
			pass

	def count_eval(self,level=1):
		ECHEC,VICTOIRE = 0,0
		for i in self.data[level]:
			if i['state']: VICTOIRE+=1
			if not i['state']: ECHEC+=1
		return (ECHEC,VICTOIRE)

	def ratio_eval(self,level=1):
		ECHEC,VICTOIRE = self.count_eval(level)
		ratio_e,ratio_v = int(100*(ECHEC/16.0)),int(100*(VICTOIRE/16.0))
		return (ratio_e,ratio_v)

	def save_timeline(self,level=1):
		self.timeline = {}
		for i in self.data[level]:
			step = i['step']
			state = i['state']
			datetimevar = i['datetime']
			self.timeline[datetimevar] = {'step':step,'state':state}
		TL = ""
		for i in self.timeline.keys():
			if not self.timeline[i]['state']:
				TL = TL + "LE SUJET ESSAYE DE RESOUDRE LE PROBLEME["+str(level)+"](etape: "+str(self.timeline[i]['step'])+").\n"

			if self.timeline[i]['state']:
				TL = TL + "LE SUJET A REUSSIT L'ETAPE:"+str(self.timeline[i]['step'])+".\n"
				#delta = self.timeline.keys()[-1] - self.timeline.keys()[0]
				#TL = TL + "IL A FINIT CETTE ETAPE EN "+str(delta.seconds)+" SECONDES.\n\n"
		
		TL = TL + "ECHEC: "+str(self.count_eval(level)[0])+" ; REUSSITE:"+str(self.count_eval(level)[1])
		TL = TL + "ECHEC: "+str(self.ratio_eval(level)[0])+"% ; REUSSITE:"+str(self.ratio_eval(level)[1])+"%"
		TL = TL + "PROGRESSION: "+str(self.ratio_progress(level))+"%\n\n"

		f = open("_tmp_/"+self.name+".dict",'w+b')
		f.write(str(self.timeline))
		f.close()
		f = open("_tmp_/"+self.name+".log",'w+b')
		f.write(TL)
		f.close()
"""
import evaluator
motorcollect = evaluator.collecteur()
motorcollect.add(level=1,step=3,state=0,datetime_=datetime.now())

motorcollect.save_timeline(level=1)
"""