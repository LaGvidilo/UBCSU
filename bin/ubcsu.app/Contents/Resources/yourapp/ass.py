#coding: utf-8
import os
#from kivy.core.audio import SoundLoader
import pygame.mixer
import ast
import io
pygame.mixer.init(frequency=44100,size=-16, channels=2, buffer=4096)
class sprites(object):
	def __init__(self):
		self.sprites = {}
		self.sequences = {}
		self.load_png("data")
		self.load_sequences("data")


	def cherche(self,dir):
	    FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
	    return FichList

	def read_file(self,file):
	    f = open(file,"r+b")
	    R=f.read()
	    f.close()
	    return R

	def load_png(self,dir="data"):
	    FL = self.cherche(dir)
	    for f in FL:
	        if (f.split('.')[1]=="png"):
	            self.sprites[f.split('.')[0]] = dir+"/"+f#io.BytesIO(self.read_file(dir+"/"+f))
	            print "Sprite: ",f,"Chargé!"

	def load_sequences(self,dir="data"):
	    FL = self.cherche(dir)
	    for f in FL:
	        if (f.split('.')[1]=="dict"):
	            self.sequences[f.split('.')[0]] = ast.literal_eval(self.read_file(dir+"/"+f))
	            print "Animation: ",f,"Chargée!"

	def get_animation(self,object_):
		return self.sequences[object_]

	def get_frames(self,object_,animation):
		return self.sequences[object_][animation]

	def get_data(self,object_,animation):
		r=[]
		for i in self.sequences[object_][animation]:
			r.append(self.sprites[i])
		return r

class musiques(object):
	def __init__(self):
		self.musiques = {}
		self.load_wav("data")

		self.current = ""

	def cherche(self,dir):
	    FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
	    return FichList

	def read_file(self,file):
	    f = open(file,"r")
	    R=f.read()
	    f.close()
	    return R

	def load_wav(self,dir_="data"):
	    FL = self.cherche(dir_)
	    R={}
	    for f in FL:
	        if (f.split('.')[1]=="wav") or (f.split('.')[1]=="mp3"):
	            self.musiques[f.split('.')[0]] = dir_+"/"+f
	            print "Musique: ",dir_+"/"+f,"Chargée!"
	            print "NAME MUSIC:",f.split('.')[0], self.musiques[f.split('.')[0]]
	    print "MUSIQUES:", self.musiques

	def justplay(self):
		self.musiques[name].play()

	def play_musique_loop(self,name,loop=True):
		try:
			pygame.mixer.music.load(self.musiques[name])
			if loop:
				pygame.mixer.init(frequency=44100,size=-16, channels=2, buffer=4096)
				#self.musiques[name].loop = True
				pygame.mixer.music.play(loops=loop,)
				print "Musique: ",name,"En lecture(boucle)...",self.musiques[name]
			else:
				pygame.mixer.music.play(loops=0)
				print "Musique: ",name,"En lecture(normal)...",self.musiques[name]
			self.current = name
		except:
			print "ERREUR MUSIQUE"
			pass

	def stop_all(self):
		self.current = ""
		for i in self.musiques.keys():
			if self.musiques[i]!=None: pygame.mixer.music.stop()

	def stop_musique(self):
		if self.current != "":
			pygame.mixer.music.stop()
			print "Musique: ",self.current,"Stop!"
			self.current = ""

	def organic_musique(self,name):
		self.stop_musique()
		self.play_musique_loop(name)

	def organic_musique_2(self,name):
		if self.current!=name:
			if pygame.mixer.music.get_pos() == 0:
				self.play_musique_loop(name)
		#print "POS SOUND(",name,"):",self.musiques[name].get_pos()

	def play_back_new(self,name):
		self.play_musique_loop(name=name,loop=False)

"""
MUSIC = assets.musiques()
MUSIC.organic_musique("")

"""

"""
from gtts import gTTS as voice
voice_=voice(text=TEXTE, lang='fr')
voice_.save("data/"+TABL[i][0].lower()+".mp3")
"""
