#coding: utf-8
import numpy as np
from gplearn.genetic import SymbolicRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.utils.random import check_random_state
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

from statistics import mean

import pickle #for savel/load to/from file

import os, mmap
import csv

"""
	GENETIC PROGRAMMING
	By R.S. 2018/03
"""

__version__ = "1.0.1a"


"""
def traduct(X,program = "y=sub(mul(add(div(div(X0, X0), sub(0.112, 0.165)), -0.785), div(mul(X0, div(X0, -0.507)), mul(X0, X0))), mul(mul(add(-0.491, 0.352), div(sub(-0.410, X0), div(0.165, div(0.501, X0)))), sub(sub(sub(X0, X0), div(-0.108, -0.261)), add(div(-0.013, sub(sub(X0, X0), 0.417)), div(X0, X0)))))"):
	for i in range(0,len(X)):
		exec("X"+str(i)+"="+str(X[i]))
	exec(program)
	print "y=",y
	exec("result="+y)
	return result

def sub(a,b):
	return "("+str(a)+"-"+str(b)+")"

def add(a,b):
	return "("+str(a)+"+"+str(b)+")"

def mul(a,b):
	return "("+str(a)+"*"+str(b)+")"

def div(a,b):
	return "("+str(a)+"/"+str(b)+")"
"""





def string_to_float(x):
    z = ""
    for i in x:
        if i in "0123456789.":
            if i == ".":
                if z.count(i)==0:
                    z=z+i
            else:
                z=z+i
    if z=="": z="0.00"
    return float(z)

def string_to_int(x):
    z = ""
    for i in x:
        if i in "0123456789":
            z=z+i
    if z=="": z="0"
    return int(z)

class dataset(object):
	def __init__(self):
		self.x,self.y = [],[]
	def add(self,x,y):
		self.x.append(x)
		self.y.append(y)
	def get(self):
		return self.x,self.y
	def new(self):
		self.x,self.y = [],[]



class GP_SymReg(object):
	def load_csv(self,filepath="test.csv",name_y='y'):
		DATA = []
		with open(filepath) as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				DATA.append(row)
		maxmult = 0
		for i in DATA[0].keys():
			if "x" in i:
				if string_to_int(i)>maxmult:
					maxmult = string_to_int(i)
		keyx = []
		for i in range(0,maxmult+1):
			keyx.append("x"+str(i))

		print "Nombre de X:",maxmult+1
		X,Y = [],[]
		for i in DATA:
			Y.append(string_to_float(i[name_y]))
			for j in keyx:
				X.append(string_to_float(i[j]))

		print "X=",X
		print "Y=",Y
		l = maxmult+1
		if maxmult>0:
			A,B = len(X)/l,l
		else:
			A,B = len(X),-1
		self.x_ = np.array(X).reshape(A,B)
		self.y_ = np.array(Y)
		print "x_=",self.x_
		print "y_=",self.y_
		

	def formula(self,formule="x0/2.0-x1/4.0"):
		rng = check_random_state(0)
		X_ = rng.uniform(-100, 100, 100).reshape(50,2)
		for i in range(0,2):
			formule = formule.replace("x"+str(i),"X_[:,"+str(i)+"]")
		print "y_ =", formule
		exec("y_="+formule)
		self.y_ = y_
		self.x_ = X_

	def set_x_y(self,x,y,multiple=1):
		rng = check_random_state(0)
		self.x_ = rng.uniform(-len(x), len(x), len(x)).reshape(len(x)/2,multiple)
		j=0
		for i in self.x_:
			for h in range(0,multiple):
				i[h] = x[j+h]
			j+=1
		self.x_ = np.array(self.x_).reshape(len(x)/2,multiple)
		self.y_ = np.array(y)
		print self.x_
		print self.y_

	def __init__(self,population_size=5000,
					generations=20, stopping_criteria=0.01,
					p_crossover=0.7, p_subtree_mutation=0.1,
					p_hoist_mutation=0.05, p_point_mutation=0.1,
					max_samples=0.9, verbose=1,
					parsimony_coefficient=0.01, random_state=0):
		self.y_ = None
		self.est_gp = SymbolicRegressor(population_size=population_size,
						generations=generations, stopping_criteria=stopping_criteria,
						p_crossover=p_crossover, p_subtree_mutation=p_subtree_mutation,
						p_hoist_mutation=p_hoist_mutation, p_point_mutation=p_point_mutation,
						max_samples=max_samples, verbose=verbose,
						parsimony_coefficient=parsimony_coefficient, random_state=random_state)

	def learn(self):
		print "ENTRAINEMENT..."
		self.est_gp.fit(self.x_,self.y_)
		print "ENTRAINEMENT TERMINE!"

	def predict(self,X=[16,19],scale=1/1000.):
		u = []
		for i in range(0,len(X)):
			u.append(np.arange(X[i], X[i]+1, scale))
		u = tuple(u)
		self.y_gp = self.est_gp.predict(np.c_[u]).reshape(u[0].shape)
		return mean(self.y_gp)

	def save(self,filepath="temp.bin"):
		f=open(filepath,'w+b')
		f.write(pickle.dumps(self.est_gp))
		f.close()
		print "MODEL SAVE!("+filepath+")"

	def load(self,filepath="temp.bin"):
		l = os.stat(filepath).st_size
		with open(filepath, 'rb') as f:
			mm = mmap.mmap(f.fileno(),0,prot=mmap.PROT_READ)
			self.est_gp = pickle.loads(mm.read(l))
		print "MODEL LOAD!("+filepath+")"

	def get_program(self):
		return self.est_gp._program

	def print_program(self):
		print self.est_gp._program

	def determination(self):
		x,y=[],[]
		x=range(-100,100+1)
		for i in x:
			y.append(f(i))
		self.set_x_y(x,y)
		print "SET DETERMINATION X(-100 to 100) Y(from f(x))."



