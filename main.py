# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.lang import Builder
from kivy.uix import actionbar
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.spinner import Spinner
from kivy.uix.accordion import Accordion
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import NumericProperty
from kivy.properties import StringProperty
from kivy.config import Config
from kivy.core.window import Window
from kivy.uix.togglebutton import ToggleButton
import random
import os.path
from datetime import datetime
from datetime import date
#import calendar
#from subprocess import call
from kivy.uix.filechooser import FileChooserController
from kivy.uix.popup import Popup
from kivy.uix.gridlayout import GridLayout
from copy import copy
from kivy.uix.checkbox import CheckBox
from kivy.core.audio import SoundLoader
from kivy.uix.switch import Switch
from kivy.uix.progressbar import ProgressBar
from kivy.properties import ListProperty, StringProperty, NumericProperty
from kivy.factory import Factory
from kivy.uix.image import Image

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

__version__ = "1.5.8"


class cheateggs(object):
	def generate(self,n=1):
		R=[]
		random.seed(str(n)+":game")
		for i in range(0,5):
			R.append(str(random.randint(0,9)))
		return "".join(R)

SECRET_CODE = cheateggs().generate() 
SECRET_MESSAGE = "Voila, tu a reussit ce jeu,\ncela te donne un bonus secret pour mon prochaint jeu.\nVoici le code secret:"+SECRET_CODE

import fraculateur as FRAC
defraculateur=FRAC.RMPEQ()

from copy import deepcopy

import assets
MUSIC = assets.musiques()
CANAL1 = deepcopy(MUSIC)
CANAL2 = deepcopy(MUSIC)
SPRITE = assets.sprites()

from datetime import datetime

#PUZZLE DATA
import autoload
class puzzle_data:
	uload = autoload.autoload()
	level1 = uload.load_ext(dir="data/puzzle",ext="png",level="1",sep="_")
	level2 = uload.load_ext(dir="data/puzzle",ext="png",level="2",sep="_")
	level3 = uload.load_ext(dir="data/puzzle",ext="png",level="3",sep="_")
	level4 = uload.load_ext(dir="data/puzzle",ext="png",level="4",sep="_")
	level5 = uload.load_ext(dir="data/puzzle",ext="png",level="5",sep="_")
	level6 = uload.load_ext(dir="data/puzzle",ext="png",level="6",sep="_")
	level7 = uload.load_ext(dir="data/puzzle",ext="png",level="7",sep="_")
	level8 = uload.load_ext(dir="data/puzzle",ext="png",level="8",sep="_")
	level9 = uload.load_ext(dir="data/puzzle",ext="png",level="9",sep="_")
	level10 = uload.load_ext(dir="data/puzzle",ext="png",level="10",sep="_")
	level11 = uload.load_ext(dir="data/puzzle",ext="png",level="11",sep="_")
	level12 = uload.load_ext(dir="data/puzzle",ext="png",level="12",sep="_")
	level13 = uload.load_ext(dir="data/puzzle",ext="png",level="13",sep="_")
	level14 = uload.load_ext(dir="data/puzzle",ext="png",level="14",sep="_")
	level15 = uload.load_ext(dir="data/puzzle",ext="png",level="15",sep="_")
	level16 = uload.load_ext(dir="data/puzzle",ext="png",level="16",sep="_")

class globvar:
	MODE_GAME = "FREE"

import memory
DBDD = memory.DBDICT()


import evaluator
#motorcollect = evaluator.collecteur()
class Game4(FloatLayout):
	def game_init(self,lvl=1):
		self.gd.clear_widgets()
		self.oldA=None
		self.oldB=None
		self.laverite = []
		self.step_current = lvl
		self.step_max = 16
		self.afflvl(lvl)
		return ""

	def afflvl(self,lvl):
		exec("A = puzzle_data.level"+str(lvl))
		A = sorted(A)
		cols,rows = int(A[-1].split("_")[2]),int(A[-1].split("_")[1])
		print "cols,rows = ",cols,rows
		self.gd.cols = cols
		self.gd.rows = rows
		for i in A:
			IMG = ButtonB(src="data/puzzle/"+i+".png")
			IMG.bind(on_press=self.select)
			self.gd.add_widget(IMG)
			self.laverite.append("data/puzzle/"+i+".png")
		self.melange()

	def melange(self):
		LIST = sorted(self.gd.children)
		for i in LIST:
			A = i
			while 1:
				Z = random.choice(sorted(self.gd.children))
				try:
					LIST[LIST.index(Z)].src,LIST[LIST.index(A)].src = A.src,Z.src
					LIST.remove(Z)
					break
				except:
					pass

	def message_n1_gamewin(self):
		A = DBDD.select(game=1)['timestamp']
		B = DBDD.select(game=4)['timestamp']
		content = ConfirmOKPopup(text=SECRET_MESSAGE+"\n"+"VOUS COMMENCEZ LE PREMIER CHECKPOINT A:"+str(A)+"\nVOUS TERMINEZ A:"+str(B))
		content.bind(on_answer=self._on_answer_n1_gamewin)
		self.popup = Popup(title="VICTOIRE !",
							content=content,
							size_hint=(None, None),
							size=(480,400),
							auto_dismiss= False)
		self.popup.open()

	def _on_answer_n1_gamewin(self, instance, answer):
		self.popup.dismiss()	
		self.init_game()
		MUSIC.organic_musique("EPIC_TITLE")
		self.parent.parent.current = "Menu"		

	def next_level(self):
		if self.step_current<self.step_max:
			self.step_current+=1
			self.game_init(self.step_current)
		if self.step_current>=self.step_max:
			DBDD.insert(level=self.step_current,game=4,timestamp=datetime.now())
			DBDD.save()
			#MESSAGE DE VICTOIRE AVEC CODE DE TRICHE
			if globvar.MODE_GAME == "RUSH":
				self.message_n1_gamewin()
				self.game_init(1)
				MUSIC.organic_musique("EPIC_TITLE")
				self.parent.parent.current = "Menu"
			else:
				MUSIC.organic_musique("EPIC_TITLE")
				self.parent.parent.current = "Menu"
				self.step_current = 1
				self.game_init(1)

	def quit(self):
		self.game_init()
		MUSIC.organic_musique("EPIC_TITLE")
		self.parent.parent.current = "Menu"

	def select(self,img):
		print "selected:",img
		if self.oldA == None:
			self.imgA = img
			self.oldA = img.src
			img.src = "data/puzzle/vide.png"
			print "SELECT A"
		elif self.oldB == None:
			self.imgB = img
			self.oldB = img.src
			img.src = "data/puzzle/vide.png"
			print "SELECT B"
		if (self.oldA!=None) and (self.oldB!=None):
			print "ECHANGE A-B"
			self.imgA.src = self.oldB
			self.imgB.src = self.oldA
			self.oldA,self.oldB = None,None
			self.detector()

	def detector(self):
		tmp = []
		for i in self.gd.children:
			#print "item:", i.src
			tmp.append(i.src)
		tmp = tmp[::-1]
		#print "VERITE: ",tmp == self.laverite
		#print ">>>",self.laverite
		#print "============"
		#print ">>>",tmp

		#motorcollect.add(level=4,step=self.step_current,state=int(tmp == self.laverite),datetime_=datetime.now())
		#motorcollect.save_timeline(level=4)
		if tmp == self.laverite:
			self.next_level()



"""
chronometre = FRAC.chrono(65)
chronometre.dec()
M,S = chronometre.get()
label.text = chronometre.get_format()
"""
import ast
class stack(object):
	def __init__(self,load=""):
		self.piles = self.load(load) if len(load)>0 else {}
		self.current = "None"

	def load(self,fichier):
		f = open(fichier,'r+b')
		return ast.literal_eval(f.read())

	def save(self, fichier):
		f = open(fichier,'w+b').write(str(self.piles))

	def make_stack(self, name='None', capacity=-1):
		self.piles[name]={'stack':[],'capacity':capacity}
		self.set_current(name)

	def set_current(self,name):
		self.current=name

	def push(self,x,name=""):
		if name == "": name = self.current
		c=self.piles[name]['capacity']
		if c < len(self.piles[name]['stack']):
			self.piles[name]['stack'].append(x)
			return True
		return False

	def pop(self,name=""):
		if name == "": name = self.current
		c = self.piles[name]['stack']
		return c.pop if len(c)>0 else None

	def is_empty(self,name=""):
		if name == "": name = self.current
		return len(self.piles[name]['stack'])<1

	def get_size(self,name=""):
		return len(self.piles[name]['stack'])

	def set_current(self,name):
		self.curent = name

	def peek(self,name=""):
		if name == "": name = self.current
		l = len(self.piles[name]['stack'])-1
		return self.piles[name]['stack'][l]

	def clear(self,name=""):
		if name == "": name = self.current
		self.piles[name]['stack'] = []

class variables:
	score=0
	level=0
	difficult=1

class ConfirmPopup(GridLayout):
	text = StringProperty()
	
	def __init__(self,**kwargs):
		self.register_event_type('on_answer')
		super(ConfirmPopup,self).__init__(**kwargs)
		
	def on_answer(self, *args):
		pass	

class ConfirmOKPopup(GridLayout):
	text = StringProperty()
	#image = StringProperty()

	
	def __init__(self,**kwargs):
		self.register_event_type('on_answer')
		super(ConfirmOKPopup,self).__init__(**kwargs)
		
	def on_answer(self, *args):
		pass


class Game1(BoxLayout):
	def message_n1_gameover(self):
		content = ConfirmPopup(text='Vous avez perdu voulez vous recommencer une partie?\nSi vous répondez non le jeu quittera!')
		content.bind(on_answer=self._on_answer_n1_gameover)
		self.popup = Popup(title="GAMEOVER !",
							content=content,
							size_hint=(None, None),
							size=(480,400),
							auto_dismiss= False)
		self.popup.open()

	def message_quit(self):
		if globvar.MODE_GAME == "FREE":
			content = ConfirmPopup(text='Vous êtes sur de vouloir quitter ?')
			content.bind(on_answer=self._on_answer_quit)
			self.popup = Popup(title="Fin de partie ?",
								content=content,
								size_hint=(None, None),
								size=(480,400),
								auto_dismiss= False)
			self.popup.open()

	def message_n1_win(self):
		#ICI LE SON VOIX POUR CEUX QUI NE SAVENT PAS LIRE
		content = ConfirmOKPopup(text='Tu a reussit!')
		content.bind(on_answer=self._on_answer_ok)
		self.popup = Popup(title="GOOD GAME !",
							content=content,
							size_hint=(None, None),
							size=(480,400),
							auto_dismiss= False)
		self.popup.open()

	def init_game(self):
		self.y1z1.state = "normal"
		self.y1z2.state = "normal"
		self.y1z3.state = "normal"
		self.y2.text = "0"
		self.y3z1.state = "normal"
		self.y3z2.state = "normal"
		self.y3z3.state = "normal"
		FRAC.x1y1 = int(0)
		FRAC.x1y2 = int(0)
		FRAC.x1y3 = int(0)
		FRAC.x1y0 = 0
		FRAC.A = 0
		FRAC.total = 0
		FRAC.power = False
		defraculateur.__init__()
		FRAC.chronometre = FRAC.chrono(65)
		self.timer.text = "0:0"
		self.powerplant.active = False
		Clock.unschedule(self.fluctuation)
		Clock.unschedule(self.microfluctuation)
		Clock.unschedule(self.loop_niv1)
		Clock.unschedule(self.loop_timer)
		self.plusmoins.active = False
		self.c.bcolor = [0,1,0,1]
		self.c.text = "0"
		self.lblvalue.text = "# [ 0 ]"
		self.lblvalue.bcolor = [.3,.3,.3,.3]

	def _on_answer_n1_gameover(self, instance, answer):
		if answer == "yes":
			self.init_game()

		if answer == "no":
			self.init_game()
			MUSIC.organic_musique("EPIC_TITLE")
			self.parent.parent.current = "Menu"

		self.popup.dismiss()

	def _on_answer_quit(self, instance, answer):
		if answer == "yes":
			self.init_game()
			#print self #Game1
			#print self.parent #Screen = Game 1
			#print self.parent.parent #scm
			#print self.parent.parent.parent #RootWidget

			MUSIC.organic_musique("EPIC_TITLE")
			self.parent.parent.current = "Menu"

		self.popup.dismiss()

	def next_level(self):
		#motorcollect.add(level=1,step=FRAC.level,state=1,datetime_=datetime.now())
		#motorcollect.save_timeline(level=1)
		self.init_game()
		FRAC.level+=1
		self.lvl.text = "Niveau "+str(FRAC.level)
		if globvar.MODE_GAME == "RUSH":
			if FRAC.level > 5:
				DBDD.insert(level=FRAC.level,game=1,timestamp=datetime.now())
				DBDD.save()
				#ici on passe a a la suite si le mode est RUSH
				self.init_game()
				#self.scm.current = 
				id_ = 3
				MUSIC.stop_all()
				if id_ == 4:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("NIV4-1")
				if id_ == 3:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("WCTEXT")
				if id_ == 2:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("mediumSong1")
				if id_ == 1:
					if FRAC.music:MUSIC.play_back_new("intro-niv-1",.4)

				self.parent.parent.current = "Game "+str(id_)
		else:
			self.game_init()
			MUSIC.organic_musique("EPIC_TITLE")
			self.parent.parent.current = "Menu"


	def _on_answer_ok(self, instance, answer):
		self.popup.dismiss()

	def fluctuation(self,obj):
		F = defraculateur.simulate_fluctuation(facteur=6)
		print "Fluctuation:", F
		self.affichage()

	def microfluctuation(self,obj):
		F = defraculateur.simulate_fluctuation(facteur=3)
		print "Micro-Fluctuation:", F
		self.affichage()

	def loop_niv1(self,obj):
		FRAC.total = int(defraculateur.C) + int(FRAC.A)
		defraculateur.atteinte(FRAC.total)
		PV = int(defraculateur.PV)
		self.pv.value = PV
		self.affichage()
		if abs(FRAC.total)>2: CANAL1.organic_musique_2("defaillance")
		if abs(FRAC.total)>9: CANAL2.organic_musique_2("alarmeFRAC")

		if defraculateur.gameisover() or FRAC.chronometre.is_null():
			self.powerplant.active = False
			Clock.unschedule(self.fluctuation)
			Clock.unschedule(self.microfluctuation)
			Clock.unschedule(self.loop_niv1)
			Clock.unschedule(self.loop_timer)
			self.message_n1_gameover()
			MUSIC.stop_all()
			CANAL1.stop_all()
			CANAL2.stop_all()


		if defraculateur.gameiswin():
			self.powerplant.active = False
			Clock.unschedule(self.fluctuation)
			Clock.unschedule(self.microfluctuation)
			Clock.unschedule(self.loop_niv1)
			Clock.unschedule(self.loop_timer)
			#self.scm.current = "niveau 2"
			#MUSIC.organic_musique("NIV2-1")
			self.next_level()
			self.message_n1_win()
			MUSIC.stop_all()
			CANAL1.stop_all()
			CANAL2.stop_all()

		else:
			pass
			#motorcollect.add(level=1,step=FRAC.level,state=0,datetime_=datetime.now())
			#motorcollect.save_timeline(level=1)

	def loop_timer(self,obj):
		FRAC.chronometre.dec()
		self.timer.text = FRAC.chronometre.get_format()

		"""
		chronometre = FRAC.chrono(65)
		chronometre.dec()
		M,S = chronometre.get()
		label.text = chronometre.get_format()
		"""


	def power(self,button):
		if button:
			Clock.schedule_interval(self.fluctuation, 5+(3+(FRAC.level*0.1)))
			Clock.schedule_interval(self.microfluctuation, 1+((FRAC.level*0.05)))
			Clock.schedule_interval(self.loop_niv1,0.1)
			Clock.schedule_interval(self.loop_timer,1)
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("NIV1-1")

		else:
			Clock.unschedule(self.fluctuation)
			Clock.unschedule(self.microfluctuation)
			Clock.unschedule(self.loop_niv1)
			Clock.unschedule(self.loop_timer)
			MUSIC.stop_all()
			CANAL1.stop_all()
			CANAL2.stop_all()

		FRAC.power = button


	def purge(self):
		if FRAC.power:
			self.y1z1.state = "normal"
			self.y1z2.state = "normal"
			self.y1z3.state = "normal"
			self.y2.text = "0"
			self.y3z1.state = "normal"
			self.y3z2.state = "normal"
			self.y3z3.state = "normal"
			FRAC.x1y1 = int(0)
			FRAC.x1y2 = int(0)
			FRAC.x1y3 = int(0)
			self.affichage()

	def y0_change(self,button):
		MUSIC.play_back_new("comp3")
		if FRAC.power:
			print button
			#FRAC.x1y0 = int()
			self.affichage()

	def y1_change(self,button,otherbuttons):
		if FRAC.power:
			print button.text,button.state
			FRAC.x1y1 = int(button.text)
			self.affichage()

	def y2_change(self,button):
		if FRAC.power:
			FRAC.x1y2 = int(button.text)
			self.affichage()

	def y3_change(self,button,otherbuttons):
		if FRAC.power:
			if button.state == "normal":
				button.state = "down"
			FRAC.x1y3 = int(button.text)
			self.affichage()

	def calcul(self):
		yy1,yy2,yy3 = 0,0,0
		if self.plusmoins.active:
			if self.y1z1.state == "down": yy1 += 1
			if self.y1z2.state == "down": yy1 += 2
			if self.y1z3.state == "down": yy1 += 3
			yy2 += int(self.y2.text)
			if self.y3z1.state == "down": yy3 += 1
			if self.y3z2.state == "down": yy3 += 2
			if self.y3z3.state == "down": yy3 += 3
		else:
			if self.y1z1.state == "down": yy1 += -1
			if self.y1z2.state == "down": yy1 += -2
			if self.y1z3.state == "down": yy1 += -3
			yy2 += -int(self.y2.text)
			if self.y3z1.state == "down": yy3 += -1
			if self.y3z2.state == "down": yy3 += -2
			if self.y3z3.state == "down": yy3 += -3
		FRAC.A = yy1+yy2+yy3
		return yy1,yy2,yy3

	def affichage(self):
		yy1,yy2,yy3 = self.calcul()
		
		if self.plusmoins.active:
			self.polarite.text = "( + )"
		else:
			self.polarite.text = "( - )"

		self.yy1.text = "( "+str(yy1)+" )"
		self.yy2.text = "( "+str(yy2)+" )"
		self.yy3.text = "( "+str(yy3)+" )"
		self.c.text = str(defraculateur.C)
		if abs(defraculateur.C) == 0: self.c.bcolor = [0,1,0,1]
		if 8 > abs(defraculateur.C) > 0 : self.c.bcolor = [1,1,0,1]
		if abs(defraculateur.C) > 8 : self.c.bcolor = [1,0,0,1]

		if abs(defraculateur.C) == 0: self.c.color = [1,1,1,1]
		if 8 > abs(defraculateur.C) > 0 : self.c.color = [0,0,0,1]
		if abs(defraculateur.C) > 8 : self.c.color = [1,1,1,1]


		if abs(FRAC.total) == 0: self.lblvalue.bcolor = [0,1,0,1]
		if 8 > abs(FRAC.total) > 0 : self.lblvalue.bcolor = [1,1,0,1]
		if abs(FRAC.total) > 8 : self.lblvalue.bcolor = [1,0,0,1]

		if abs(FRAC.total) == 0: self.lblvalue.color = [1,1,1,1]
		if 8 > abs(FRAC.total) > 0 : self.lblvalue.color = [0,0,0,1]
		if abs(FRAC.total) > 8 : self.lblvalue.color = [1,1,1,1]
		self.lblvalue.text = " ("+str(FRAC.A)+") + ("+str(defraculateur.C)+") === "+str(FRAC.total)

import os
from FluoSQL import captcha
from FluoSQL import autoload
class Game2(BoxLayout):
	#Clock.schedule_interval(self.loop_mortys,2)
	#Clock.schedule_interval(self.loop_mortys_spawn,0.5)

	def game_init(self):
		AL = autoload()
		files = AL.cherche("captcha_")
		for i in files:
			os.remove("captcha_/"+i)
		self.step_current = 0
		self.step_max = 16
		self.CAPTCHA = captcha(self.step_current)
		self.data = self.CAPTCHA.make_file(1,3+(self.step_current),380,300,(200,120,126))[0]
		#REPONSE ATTENDU DANS self.data['text']
		self.reponse.text = ""
		self.score.text = str(self.step_current)+"/"+str(self.step_max)
		self.captcha.source = self.data['file']
		print "GAME 2 LOADED"
		return str(self.step_current)+"/"+str(self.step_max)

	def next_level(self):
		if self.step_current<self.step_max:
			self.step_current+=1
			self.score.text = str(self.step_current)+"/"+str(self.step_max)
			self.next_captcha()
			self.reponse.text = ""
		else:
			DBDD.insert(level=self.step_current,game=2,timestamp=datetime.now())
			DBDD.save()
			#ici niveau suivant
			if globvar.MODE_GAME == "RUSH":
				id_ = 2
				MUSIC.stop_all()
				if id_ == 4:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("NIV4-1")
				if id_ == 3:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("WCTEXT")
				if id_ == 2:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("mediumSong1")
				if id_ == 1:
					if FRAC.music:MUSIC.play_back_new("intro-niv-1",.4)

				self.parent.parent.current = "Game "+str(id_)
			else:
				self.game_init()
				MUSIC.organic_musique("EPIC_TITLE")
				self.parent.parent.current = "Menu"

	def next_captcha(self):
		os.remove(self.data['file'])
		self.data = self.CAPTCHA.make_file(1,3+(self.step_current),380,300,(200,120,126))[0]
		self.captcha.source = self.data['file']
		print "CAPTCHA: ",self.data['text']

	def validate(self,reponse_):
		#motorcollect.add(level=2,step=self.step_current,state=int(self.data['text']==reponse_),datetime_=datetime.now())
		#motorcollect.save_timeline(level=2)
		if self.data['text'] == reponse_:
			self.next_level()
		else:
			self.reponse.text = ""

	def quit(self):
		if globvar.MODE_GAME == "FREE":
			self.game_init()
			MUSIC.organic_musique("EPIC_TITLE")
			self.parent.parent.current = "Menu"

import statistics
#from faker import Faker

AUTOLOAD = autoload()
mots = AUTOLOAD.read_file("data/mots.txt").split('\n')

"""
from google import google#Google-Search-API
def google_search(search, num_page=1, single=False):
	search_results = google.search(search, num_page)
	RESULTS = []
	for result in search_results:
		RESULTS.append(result.description)
		if single: break
	return RESULTS
"""

class Game3(BoxLayout):
	def generate_text(self):
		if variables.difficult == 1:
			TEXT = ""
			for i in range(0,2):
				TEXT = TEXT +random.choice(mots).lower()+" "
		if variables.difficult == 2:
			TEXT = ""
			for i in range(0,4):
				TEXT = TEXT +random.choice(mots).lower()#self.fake.sentence().lower()

		choix = random.choice(TEXT.split("."))
		self.reponseatt=choix
		AFFTEXT = TEXT.replace(choix,"[color=00ff00]"+choix+"[/color]")
		self.texte.markup = True
		self.texte.text = AFFTEXT.replace(".",'\n')
		print "GAME 3 LOADED"

	def game_init(self):
		#self.fake = Faker()
		self.performance = []
		self.A = []
		self.step_current = 0
		self.step_max = 6
		self.generate_text()
		return str(self.step_current)+"/"+str(self.step_max)

	def next_level(self):
		if self.step_current<self.step_max:
			self.reponse.text =""
			self.step_current+=1
			self.generate_text()
			self.level.text = str(self.step_current)+"/"+str(self.step_max)
		else:
			DBDD.insert(level=self.step_current,game=3,timestamp=datetime.now())
			DBDD.save()
			#ici le changement de niveau
			if globvar.MODE_GAME == "RUSH":
				id_ = 4
				MUSIC.stop_all()
				if id_ == 4:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("NIV4-1")
				if id_ == 3:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("WCTEXT")
				if id_ == 2:
					#MUSIC.play_back_new("teleportation")
					if FRAC.music: MUSIC.organic_musique("mediumSong1")
				if id_ == 1:
					if FRAC.music:MUSIC.play_back_new("intro-niv-1")

				self.parent.parent.current = "Game "+str(id_)
			else:
				self.game_init()
				MUSIC.organic_musique("EPIC_TITLE")
				self.parent.parent.current = "Menu"

	def validate(self):
		rep = self.reponse.text
		if self.A == []:
			self.A.append(datetime.now())
		
		#motorcollect.add(level=3,step=self.step_current,state=int(rep==self.reponseatt),datetime_=datetime.now())
		#motorcollect.save_timeline(level=3)
		if rep == self.reponseatt:
			self.next_level()
			if len(self.A)>0:
				self.A.append(datetime.now())
				A,B = self.A[0],self.A[1]
				C = B-A
				SEC = int(round(C.total_seconds()))
				self.score.text = str(SEC)+"s"
				self.performance.append(SEC)
				self.A = []

	def quit(self):
		if globvar.MODE_GAME == "FREE":
			self.game_init()
			MUSIC.organic_musique("EPIC_TITLE")
			self.parent.parent.current = "Menu"



class LabelB(Label):
  bcolor = ListProperty([1,1,1,1])

Factory.register('KivyB', module='LabelB')

class ButtonB(Button):
  src = StringProperty("")

Factory.register('KivyB', module='ButtonB')

class RootWidget(BoxLayout):
	def quit(self):
		sys.exit(0)

	def back(self):
		self.scm.current = "Menu"
		SMenu.init(FRAC.mouse_x,FRAC.mouse_y,root=self)

	def options(self):
		self.scm.current = "Options"
		SMenu.init(FRAC.mouse_x,FRAC.mouse_y,root=self)

	def campagne(self):
		globvar.MODE_GAME = "RUSH"
		DBDD.insert(level=1,game=1,timestamp=datetime.now())
		DBDD.save()
		id_ = 1
		self.scm.current = "Game "+str(id_)
		MUSIC.stop_all()
		if id_ == 4:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("NIV4-1")
		if id_ == 3:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("WCTEXT")
		if id_ == 2:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("mediumSong1")
		if id_ == 1:
			if FRAC.music:MUSIC.play_back_new("intro-niv-1")

	def games(self):
		self.scm.current = "GamesMenu"
		SMenu.init(FRAC.mouse_x,FRAC.mouse_y,root=self)

	def game_select(self,id_):
		globvar.MODE_GAME = "FREE"
		self.scm.current = "Game "+str(id_)
		MUSIC.stop_all()
		if id_ == 4:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("NIV4-1")
		if id_ == 3:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("WCTEXT")
		if id_ == 2:
			#MUSIC.play_back_new("teleportation")
			if FRAC.music: MUSIC.organic_musique("mediumSong1")
		if id_ == 1:
			if FRAC.music:MUSIC.play_back_new("intro-niv-1")

	def opt_music(self,state):
		FRAC.music = state
		if state:
			if FRAC.music: MUSIC.organic_musique("EPIC_TITLE")
		else:
			MUSIC.stop_all()

	def action(self,obj,type_ = 0):
		print FRAC.mouse_x,FRAC.mouse_y
		SMenu.init(FRAC.mouse_x,FRAC.mouse_y,obj,self)
		SMenu.load_action(type_,obj=self)

	def message(self,message):
		#ICI LE SON VOIX POUR CEUX QUI NE SAVENT PAS LIRE
		content = ConfirmOKPopup(text=message)
		content.bind(on_answer=self._on_answer_ok)
		self.popup = Popup(title="Info",
							content=content,
							size_hint=(None, None),
							size=(480,400),
							auto_dismiss= False)
		self.popup.open()

	def startgame2(self,message):
		#ICI LE SON VOIX POUR CEUX QUI NE SAVENT PAS LIRE
		content = ConfirmOKPopup(text=message)
		content.bind(on_answer=self._on_answer_start)
		self.popup = Popup(title="Info",
							content=content,
							size_hint=(None, None),
							size=(480,400),
							auto_dismiss= False)
		self.popup.open()

	def _on_answer_ok(self, instance, answer):
		self.popup.dismiss()

	def _on_answer_start(self, instance, answer):
		self.popup.dismiss()
		FRAC.Mortys = True


class sim_menu(object):
	def init(self,pos_x,pos_y,obj=None,root=None):
		if root!=None: self.root_ = root
		self.x,self.y = pos_x,pos_y
		if obj!=None: self.obj = obj
		try:
			if len(self.temp_mem)>0:
				for i in self.temp_mem:
					self.root_.menugame.remove_widget(i)
			self.temp_mem = []
		except:
			self.temp_mem = []

	def kill(self):
		try:
			if len(self.temp_mem)>0:
				for i in self.temp_mem:
					self.root_.menugame.remove_widget(i)
			self.temp_mem = []
		except:
			self.temp_mem = []

	def event_action(self):
		pass

	def load_action(self,action=0,obj=None):
		self.options = {}
		if action == 0:
			self.options = {'A propos':self.aboutgame,'Développeur':self.aboutdev,
					'Contact':self.contact, 'Annuler':self.cancel}

		self.affiche(obj)

	def affiche(self,obj):
		positions = [(0,-80),(+80,0),(0,+80),(-80,0)]
		for i in self.options.keys():
			x,y = positions.pop()
			button = Button(text=i, pos=[FRAC.mouse_x+x,FRAC.mouse_y+y],
				size=[80,80], size_hint=[None, None])
			button.bind(on_release=self.options[i])
			self.temp_mem.append(button)
			obj.menugame.add_widget(button)

	def cancel(self,obj):
		if len(self.temp_mem)>0:
			for i in self.temp_mem:
				self.root_.menugame.remove_widget(i)
		self.temp_mem = []

	#EVENTS MENU SIM
	def aboutgame(self,obj):
		self.root_.message("UltimateBrainCchallenge for SuperUser \nVersion "+__version__+"\nCréé en 2018 pour la MACJAM #1")
		self.kill()

	def aboutdev(self,obj):
		self.root_.message("Par: Rick Sanchez en mars 2018.\nMusique: Rick S. & DYLZAL.\nNiveau Puzzle, Images: Léo Endor.\nRick est aussi le créateur de informabox.fr")
		self.kill()

	def contact(self,obj):
		self.root_.message("Rick Sanchez email: informabox.contact@gmail.com")
		self.kill()
		
		

def set_mouse_pos(self,pos):
	FRAC.mouse_x,FRAC.mouse_y = pos

SMenu = sim_menu()
class MainApp(App):
	'''This is the main class of your app.
	Define any app wide entities here.
	This class can be accessed anywhere inside the kivy app as,
	in python::

	 app = App.get_running_app()
	 ####print (app.title)

	in kv language::

	 on_release: ####print(app.title)
	Name of the .kv file that is auto-loaded is derived from the name
	of this class::

	 MainApp = main.kv
	 MainClass = mainclass.kv

	The App part is auto removed and the whole name is lowercased.
	'''
	def _on_answer(self, instance, answer):
		self.popup.dismiss()
		
	def on_start(self): # DEMARRAGE, SI LA BASE N EXISTE PAS ON LA CREE AVEC CES TABLES
		MUSIC.organic_musique("EPIC_TITLE")
        Window.bind(mouse_pos=set_mouse_pos)
		
	def build(self):
		self.icon = 'data/icon.png'
		return RootWidget()


if __name__ == '__main__':
    Window.size = (926,620)
    #Config.keyboard_mode='systemanddock'
    MainApp().run()